import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    results: []
}

const reducer = (state = initialState, action) => {

    switch( action.type ){

        case actionTypes.STORE_RESULT:
            //concat returns a new array with old values plus a new value
            return updateObject(state, {results: state.results.concat({id: new Date(), value: action.result})})

        case actionTypes.DELETE_RESULT:
            const id = action.id;
            
            //const newResults = state.results.concat();
            
            //const newResults = [...state.results];
            //newResults.splice(id,1);
            
            const newResults = state.results.filter(( result ) => result.id !== id);

            //console.log(newResults);
            //console.log(state.results);

            return updateObject(state, {results: newResults});

        default:
            break;
    }


    
    return state;
}

export default reducer;